#!/bin/bash

ACME_SERVER=https://acme.turingsign.com/directory?type=DV

TS_DIR=./TuringSign
WORK_DIR=$TS_DIR/work
LOGS_DIR=$TS_DIR/logs
CONF_DIR=$TS_DIR/conf

if [ ! -x "$(which certbot)" ]; then
   echo You have to install certbot
   exit 1
fi

CERTBOT_ARGS=()

function parse_eab_credentials()
{
    PYTHONIOENCODING=utf8
    TS_EAB_KID=$(echo $1 | python3 -c "import sys, json; print(json.load(sys.stdin)['eab_kid'])")
    TS_EAB_HMAC_KEY=$(echo $1 | python3 -c "import sys, json; print(json.load(sys.stdin)['eab_hmac_key'])")
    CERTBOT_ARGS+=(--eab-kid "$TS_EAB_KID" --eab-hmac-key "$TS_EAB_HMAC_KEY" --server "$ACME_SERVER")
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --email|-m)
           TS_EMAIL="${2}"
           CERTBOT_ARGS+=(-m "${2}")
           shift
        ;;
        *) CERTBOT_ARGS+=("$1")
        ;;
    esac
    shift
done

set -- "${CERTBOT_ARGS[@]}"

if [[ -n $TS_EMAIL ]]; then
    parse_eab_credentials "$(curl -s https://api.apt-get.co.kr/acme/eab-credentials-email -H "Content-Type: application/json" --data "{\"email\":\"$TS_EMAIL\"}")"
    CERTBOT_ARGS+=( -v --work-dir "$WORK_DIR" --logs-dir "$LOGS_DIR" --config-dir "$CONF_DIR")
else
    echo "Don't exist Eamil, Pleas using --email.";
    exit;
fi

#echo "${CERTBOT_ARGS[@]}"
certbot -v "${CERTBOT_ARGS[@]}"